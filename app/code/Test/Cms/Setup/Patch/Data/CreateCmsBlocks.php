<?php

namespace Test\Cms\Setup\Patch\Data;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Cms\Api\BlockRepositoryInterface;
use Magento\Cms\Model\Block;
use Magento\Cms\Model\BlockFactory;
use Psr\Log\LoggerInterface;
use Magento\Framework\App\Config\ConfigResource\ConfigInterface;

/**
 * Class CreateFooterCmsBlock
 */
class CreateCmsBlocks implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var BlockRepositoryInterface
     */
    private $blockRepository;

    /**
     * @var BlockFactory
     */
    private $blockFactory;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ConfigInterface
     */
    private $configInterface;

    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param BlockRepositoryInterface $blockRepository
     * @param BlockFactory $blockFactory
     * @param LoggerInterface $logger
     */
    public function __construct(
        ConfigInterface $configInterface,
        ModuleDataSetupInterface $moduleDataSetup,
        BlockRepositoryInterface $blockRepository,
        BlockFactory $blockFactory,
        LoggerInterface $logger
    )
    {
        $this->configInterface = $configInterface;
        $this->moduleDataSetup = $moduleDataSetup;
        $this->blockRepository = $blockRepository;
        $this->blockFactory = $blockFactory;
        $this->logger = $logger;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $cmsBlockData = [
            [
                'title' => 'Footer Bottom copyright',
                'identifier' => 'footer-bottom-copyright',
                'content' => '
                <div class="footer-bottom-copyright">
                     <span>© Copyright 2020 Hudson shoes</span>
                </div>',
                'is_active' => 1,
                'stores' => [0],
                'sort_order' => 0
            ],
            [
                'title' => 'Footer menu',
                'identifier' => 'footer-nav-menu',
                'content' => '
                <div class="footer-nav__menu">
                    <div class="footer-nav__column">
                        <h4>Shop</h4>
                        <ul>
                           <li><a href="#">Shop mens shoes</a></li>
                           <li><a href="#">Shop mens loafers</a></li>
                           <li><a href="#">Shop mens trainers</a></li>
                           <li><a href="#">Shop mens accessories</a></li>
                           <li><a href="#">Shop womens shoes</a></li>
                           <li><a href="#">Shop womens loafers</a></li>
                           <li><a href="#">Shop womens trainers</a></li>
                           <li><a href="#">Shop womens accessories</a></li>
                        </ul>
                    </div>
                    <div class="footer-nav__column">
                        <h4>About us</h4>
                        <ul>
                           <li><a href="#">Our story</a></li>
                           <li><a href="#">Terms</a></li>
                           <li><a href="#">Privacy policy</a></li>
                           <li><a href="#">Contact</a></li>
                        </ul>
                    </div>
                    <div class="footer-nav__column">
                        <h4>Support</h4>
                        <ul>
                           <li><a href="#">Aftercare</a></li>
                           <li><a href="#">Gift cards</a></li>
                           <li><a href="#">Student discounts</a></li>
                           <li><a href="#">FAQs</a></li>
                           <li><a href="#">Returns</a></li>
                           <li><a href="#">Delivery</a></li>
                        </ul>
                    </div>
                </div>',
                'is_active' => 1,
                'stores' => [0],
                'sort_order' => 0
            ],
            [
                'title' => 'Home main Banner',
                'identifier' => 'home-main-banner',
                'content' => '
                <div class="home-banner">
                     <div class="home-banner__img">
                        <img alt="Home main Banner" src="{{view url=\'images/banners/Beryl_Homepage_Desktop.jpg\'}}"/>
                     </div>
                </div>',
                'is_active' => 1,
                'stores' => [0],
                'sort_order' => 0
            ]
        ];

        $this->moduleDataSetup->startSetup();

        foreach ($cmsBlockData as $data) {
            try {
                /** @var Block $block */
                $block = $this->blockFactory->create();
                $block->setData($data);
                $this->blockRepository->save($block);
            } catch (\Exception $exception) {
                continue;
            }
        }

        $this->moduleDataSetup->endSetup();
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }

    /**
    * Assign Theme
    *
    * @return void
    */
    protected function assignTheme()
    {
        $this->configInterface->saveConfig('design/theme/theme_id', 4, 'stores', [0]);
    }
}
