<?php
/**
 * Copyright © 2019 mediaspa. All rights reserved.
 * See COPYING.txt for license details.
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::THEME,
    'frontend/Test/base',
    __DIR__
);
