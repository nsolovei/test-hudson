define([
    'jquery',
    'ko',
    'Magento_Checkout/js/model/step-navigator'
], function($, ko, stepNavigator) {
    'use strict';

    var mixin = {
        defaults: {
            template: 'Magento_Checkout/my-shipping'
        },
        initialize: function() {
            this._super();
            $.each(stepNavigator.steps(), function(index, step) {
                if (step.code === 'shipping') {
                    step.title = 'Shipping Address';
                }
                if (step.code === 'payment') {
                    step.title = 'Billing';
                }
            });
        },
        setShippingInformation: function () {
            if (this.validateShippingInformation()) {
                stepNavigator.next();
            }
        },
    };

    return function(target) {
        return target.extend(mixin);
    }
});