define(
[
    'jquery',
    'underscore',
    'Magento_Ui/js/form/form',
    'ko',
    'Magento_Customer/js/model/customer',
    'Magento_Customer/js/model/address-list',
    'Magento_Checkout/js/model/address-converter',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/action/create-shipping-address',
    'Magento_Checkout/js/action/select-shipping-address',
    'Magento_Checkout/js/model/shipping-rates-validator',
    'Magento_Checkout/js/model/shipping-address/form-popup-state',
    'Magento_Checkout/js/model/shipping-service',
    'Magento_Checkout/js/action/select-shipping-method',
    'Magento_Checkout/js/model/shipping-rate-registry',
    'Magento_Checkout/js/action/set-shipping-information',
    'Magento_Checkout/js/model/step-navigator',
    'Magento_Ui/js/modal/modal',
    'Magento_Checkout/js/model/checkout-data-resolver',
    'Magento_Checkout/js/checkout-data',
    'uiRegistry',
    'mage/translate',
    'Magento_Checkout/js/model/shipping-rate-service'
],
function (
    $,
    _,
    Component,
    ko,
    customer,
    addressList,
    addressConverter,
    quote,
    createShippingAddress,
    selectShippingAddress,
    shippingRatesValidator,
    formPopUpState,
    shippingService,
    selectShippingMethodAction,
    rateRegistry,
    setShippingInformationAction,
    stepNavigator,
    modal,
    checkoutDataResolver,
    checkoutData,
    registry,
    $t
) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Magento_Checkout/my-step-view'
        },
        //add here your logic to display step,
        isVisible: ko.observable(false),
        errorValidationMessage: ko.observable(false),
        isFormInline: addressList().length == 0,

        /**
        *
        * @returns {*}
        */
        initialize: function () {
            this._super();

            stepNavigator.registerStep(
                'opc-shipping_method',
                null,
                'Shipping Methods',
                this.isVisible,
                _.bind(this.navigate, this),
                17
            );

            return this;
        },

        navigate: function () {

        },

        navigateToNextStep: function () {
            stepNavigator.next();
        },


        rates: shippingService.getShippingRates(),
        isLoading: shippingService.isLoading,

        setShippingInformation: function () {
            setShippingInformationAction().done(
                function () {
                    stepNavigator.next();
                }
            );   
        },

    });
}
);